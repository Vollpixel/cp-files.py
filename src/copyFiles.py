#!/usr/bin/env python3
# -*- coding: utf-8 -*-
##########################################################################
_FileName = "copyFiles.py"
##########################################################################
#
# Description:  Copy files form a local system to a test system.
#
#   v1.0        21-Jul-2006  Original version
#   v1.1        19-Apr-2013  Allow local copies without SSH
#   v1.2        23-Apr-2013  Use specific port for SCP
#   v1.2.1      24-Apr-2013  Enable SSH automatically
#   v2.0        31-Jul-2013  Remove dependency to psCfgData2
#   v2.1        14-Aug-2013  Allow execution of commands
#   v2.2        28-Oct-2013  List configurations for selection
#   v2.2.1      08-Jan-2014  Allow multi-line file lists
#   v2.3        07-May-2014  Check existence of source files
#   v2.3.1      08-May-2014  Make it more Windows ready (still incomplete)
#   v3.0        04-Dec-2014  Refactored,
#                            Allow different target file name,
#                            Improved error handling
#   v3.1        18-Dec-2014  Allow specification of explicit config file,
#                            Should be usable on Windows now (not tested)
#   v3.2        23-Nov-2016  Fixed problems with the use for local copies
#   v3.3        22-Dec-2017  Allow specification of system credentials
#                            on commandline
#
_Author = "Peter Stoldt"
_Version = "v3.3"
_LastModified = "22-Dec-2017"
#
############################################################################

Version = "{0} {1} ({2}) by {3}".format(_FileName, _Version, _LastModified, _Author)

############################################################################
# Planned Features
# ----------------
#
#  1. Allow to import other config files in current config file
#
############################################################################

import sys
import os
import platform
import argparse
import subprocess
import json
import glob

from configparser import ConfigParser, ExtendedInterpolation, DuplicateSectionError

LIN_CONF_PATH = ".config/cpFiles"
WIN_CONF_PATH = ".cpFiles"

# Linux
CP_CMD_PATTERN = "/usr/bin/cp {SourceFile} {TargetFile}"
SCP_CMD_PATTERN = "/usr/bin/scp -P {Port} {SourceFile} {User}@{TargetSystem}:{TargetFile}"
SSH_CMD_PATTERN = "/usr/bin/ssh -p {Port} {User}@{TargetSystem} '{Command}'"

# Windows
WIN_CP_CMD_PATTERN = "C:\Windows\system32\\xcopy.exe {SourceFile} {TargetFile}"
WIN_SCP_CMD_PATTERN = "C:\putty\pscp.exe -P {Port} {SourceFile} {User}@{TargetSystem}:{TargetFile}"
WIN_SSH_CMD_PATTERN = "C:\putty\putty.exe -p {Port} {User}@{TargetSystem} '{Command}'"

###################################################

ExampleConfig = """###############################################################
#
# Example cpFile configuration
#
# Required default parameter, used for the connection
#
#   [defaults]
#   TargetSystem = <system_name>   or '' for local copy
#   Port         = <ssh_port>      or '' for local copy
#   User         = <user>          or '' for local copy
#
# Settings for the use in the sections via '${my:<name>}'
#
#   [my]
#   <name>       = <value>
#   CompDir      = /data/svn-src/OMi/trunk/hpsw-opr/components
#   BsmDir       = /opt/HP/BSM
#   CDrive       = /cygdrive/c
#     :
#
# File sections. The section can have any name except of 'my'
#
#   [<section>]
#   FromDir      = <from_dir>
#   ToDir        = <target_dir>
#   Files        = <file1> <file2> ...        or
#                  <name> : <new_name>, ...   but not both!
#   Commands     = "<command>" or
#                  ["<command>", ...]
#
# File Name Notes:
#
#   If the target file name equals the source file name, you
#   may leave the target name empty. Example
#
#     <fname> :
#
#   If using <source>:<target> syntax, use a ',' as separator
#   when specifying more than one file
#
#   Files can be defined spanning multiple lines, as long as
#   they are indented deeper than the first line of the value
#
#   Simple list only:
#      Embrace file names containing spaces with "'"
#
###############################################################

[DEFAULT]
# Target system FQDN or IP-Address
# Leave this empty for local operations
TargetSystem = 16.58.78.113

# SSH port. Leave empty for local copy
Port = 12218

# User for SCP. Leave empty for local copy
User = Administrator

# Note: The settings of the DEFAULT section can be overwritten in
#       other sections, e.g. to distribute files to more than one
#       system.

[my]
# Definitions for the use in the sections
JavaDir      = /data/svn-src/hpsw-opr-fnd/trunk/platform/java
BsmDir       = /cygdrive/c/HPBSM
WorkDir      = /data/Work
AdminDir     = /cygdrive/c/Users/Administrator

BuildVers    = -10.00.150-SNAPSHOT

##### Sections ################################################

[dbexplorer]
FromDir  = ${my:JavaDir}/plt1/srvcs_dbcore/dbexplorer/target
ToDir    = ${my:BsmDir}/lib
Files    = dbexplorer${my:BuildVers}.jar : dbexplorer.jar

[links]
FromDir  = ${my:WorkDir}/Files/windows/Desktop
ToDir    = ${my:AdminDir}/Desktop
Files    = *.lnk *.url

[jEdit]
FromDir  = ${my:WorkDir}/Files/windows
ToDir    = ${my:AdminDir}/AppData/Roaming
Files    = jEdit.tar.gz
Commands = "cd ${ToDir} && tar xfz jEdit"

[TopoSync Testdata]
FromDir  = ${my:WorkDir}/OMi/Topo
ToDir    = ${my:HomeDir}
Files    = testclient.tar.gz
           TestConfig.tar.gz
Commands = ["cd ${my:HomeDir} && tar xfz testclient.tar.gz",
            "cd ${my:HomeDir} && tar xfz TestConfig.tar.gz"]
"""


class CopyFiles(ConfigParser):

    def __init__(self):

        # Initialize the super-class and use ExtendedInterpolation
        super(ConfigParser, self).__init__(interpolation = ExtendedInterpolation())

        # General data
        self.version = Version
        self.example_config = ExampleConfig
        self.file_list = []
        self.commands = {}
        self.status_is_ok = True

        self.host = ""
        self.port = 0
        self.user = ""

        # Platform dependent settings
        if platform.system() == 'Linux':
            try:
                home_dir = os.environ["HOME"]
                self.config_dir = os.path.join(home_dir, LIN_CONF_PATH)
            except:
                print("\nError: Cannot detect user home directory for configuration files.")
                raise
            self.ssh_cmd_pattern = SSH_CMD_PATTERN
            self.scp_cmd_pattern = SCP_CMD_PATTERN
            self.cp_cmd_pattern = CP_CMD_PATTERN
        else:
            try:
                home_dir = os.environ["HOMEPATH"]
                self.config_dir = os.path.join(home_dir, WIN_CONF_PATH)
            except:
                print("\nError: Cannot detect user home directory for configuration files.")
                raise
            self.ssh_cmd_pattern = WIN_SSH_CMD_PATTERN
            self.scp_cmd_pattern = WIN_SCP_CMD_PATTERN
            self.cp_cmd_pattern = WIN_CP_CMD_PATTERN

        self.example_file = os.path.join(self.config_dir, "example")
        self.config_file = ""

        # Check if settings directory exists
        if not os.path.exists(self.config_dir):
            try:
                os.mkdir(self.config_dir, 0o770)
            except:
                print("\nError: Problem with the directory '{0}'".format(self.config_dir))
                raise

        # Create default file if it does not exist
        try:
            if not os.path.exists(self.example_file):
                df = open(self.example_file, "w")
                df.write(self.example_config)
                df.close()
        except:
            print("Unexpected error:", sys.exc_info()[0], file = sys.stderr)
            raise


    def print_example_config(self):
        """Print example configuration"""

        print(self.example_config)


    def get_file_list(self, from_dir, to_dir, config_files, section):
        """Convert a string into a list of files"""

        self.file_list = []

        raw_files = config_files.replace("\n", " ")

        if raw_files.find(':') >= 0:
            self.get_file_list_new(from_dir, to_dir, raw_files, section)
        else:
            self.get_file_list_classic(from_dir, to_dir, raw_files)


    def get_file_list_new(self, from_dir, to_dir, raw_files, section):
        """Get list fo files with its target names"""

        tmp_flist = raw_files.split(',')

        for tmpf in tmp_flist:
            file_info = tmpf.split(':')

            if len(file_info) > 1:
                source_file = os.path.join(from_dir, file_info[0].strip())
                if source_file.find('*') >= 0 or source_file.find('?') >= 0:
                    target_file = to_dir
                else:
                    target_file = os.path.join(to_dir, file_info[1].strip())

                self.file_list.append([source_file, target_file])
            else:
                print("\nNote: Empty file definition found in section [{0}]".format(section))


    def get_file_list_classic(self, from_dir, to_dir, raw_files):
        """Get list fo files the classic way"""

        tmp_file = ""
        within_quotes = False

        for i in range(len(raw_files)):
            if raw_files[i] == " ":
                if within_quotes:
                    tmp_file += raw_files[i]
                    continue
                else:
                    source_file = os.path.join(from_dir, tmp_file)
                    self.file_list.append([source_file, to_dir])
                    tmp_file = ""
            elif raw_files[i] in ["'", "\""]:
                within_quotes = not within_quotes
            else:
                tmp_file += raw_files[i]

        if len(tmp_file):
            source_file = os.path.join(from_dir, tmp_file)
            self.file_list.append([source_file, to_dir])


    def source_files_exist(self):
        """Check if all configured source files exist"""

        missed_files = []

        for file in self.file_list:
            file_name = file[0]
            if '*' not in file_name:
                if len(glob.glob(file_name)) == 0:
                    missed_files.append(file_name)

        if len(missed_files):
            print()
            print("The following files do not exist:")
            print("===============================================================")
            for full_name in missed_files:
                print(full_name)
            print("===============================================================")
            return False

        return True


    def create_commands(self):
        """Deploy the files to the target system and execute commands"""

        self.commands = dict()

        for section in self.sections():
            if section == "my":
                continue

            system_config = dict()

            if self.host == "":
                system_config['TargetSystem'] = self.get('my', 'TargetSystem', fallback = None)
            else:
                system_config['TargetSystem'] = self.host
            if self.port == 0:
                system_config['Port'] = self.get('my', 'Port', fallback = None)
            else:
                system_config['Port'] = self.port
            if self.user == "":
                system_config['User'] = self.get('my', 'User', fallback = None)
            else:
                system_config['User'] = self.user

            if not system_config['TargetSystem']:
                use_ssh = False
            else:
                use_ssh = True

            cmd_list = list()

            from_dir = self.get(section, "FromDir", fallback = None)
            to_dir = self.get(section, "ToDir", fallback = None)
            files = self.get(section, "Files", fallback = None)
            commands = self.get(section, "Commands", fallback = None)

            # Files
            if from_dir:
                self.get_file_list(from_dir, to_dir, files, section)

                if not self.source_files_exist():
                    self.status_is_ok = False
                    return

                for file in self.file_list:
                    config = system_config
                    config['SourceFile'] = file[0]
                    config['TargetFile'] = file[1]

                    if use_ssh:
                        cp_cmd = self.scp_cmd_pattern.format(**config)
                    else:
                        cp_cmd = self.cp_cmd_pattern.format(**config)

                    cmd_list.append(cp_cmd)

            # Commands
            if commands:
                cmds = list()

                loaded_cmds = json.loads(commands)
                if isinstance(loaded_cmds, str):
                    cmds.append(loaded_cmds)
                else:
                    cmds.extend(loaded_cmds)

                for cmd in cmds:
                    config = system_config
                    config['Command'] = cmd

                    if use_ssh:
                        exec_cmd = self.ssh_cmd_pattern.format(**config)
                    else:
                        exec_cmd = "{0}".format(cmd)

                    cmd_list.append(exec_cmd)

            if len(cmd_list):
                self.commands[section] = cmd_list.copy()


    def execute_commands(self, test_only=False):
        """Execute the collected commands"""

        if not self.status_is_ok:
            print("Error: Overall status is not OK - Abborting...")
            return

        if test_only:
            print("# The commands in the order of its execution")
            print("# Configuration: '{0}'".format(self.config_file))

        for section in self.sections():

            if section == "my":
                continue

            print()
            print("#### [{0}] ####".format(section))

            for command in self.commands[section]:
                print(command)
                if not test_only:
                    subprocess.call(command, shell = True)
                    # pass


    def get_config_file(self):
        """Present a list of config files and return the selected one"""

        config_files = []

        filenames = os.listdir(self.config_dir)
        for fname in filenames:
            if os.path.isfile(os.path.join(self.config_dir, fname)) and fname != "example" and fname[-1] != '~':
                config_files.append(fname)

        config_files.sort()

        print()
        for i in range(len(config_files)):
            print("  {0}: {1}".format(i, config_files[i]))
        print()
        var = input("Please select configuration: ")
        print()

        self.config_file = os.path.join(self.config_dir, config_files[int(var)])

        return self.config_file


    def run_as_tool(self):
        """Run as commandline tool"""

        parser = argparse.ArgumentParser(description = "A useful tool to copy files to another system" +
                                         "and to execute commands on another system.",
                                         epilog = "Example: {0} [<config>]".format(_FileName))
        parser.add_argument('-f', '--file',
                            help = "Use configuration file 'FILE'")
        parser.add_argument('-e', '--example', action = 'store_true',
                            help = "Print example configuration")
        parser.add_argument('-o', '--host',
                            help = "Hostname or IP of target host")
        parser.add_argument('-p', '--port',
                            help = "Port on the target host")
        parser.add_argument('-u', '--user',
                            help = "User on the target host")
        parser.add_argument('-t', '--test', action = 'store_true',
                            help = "Do a test run only. Prints the commands that will be executed." +
                            "Note: The output can be redirected into a script.")

        args, opts = parser.parse_known_args()

        # print("args: {0}".format(args))
        # print("opts: {0}".format(opts))

        if args.example:
            self.print_example_config()
            sys.exit(0)
        if args.host:
            self.host = args.host
        if args.port:
            self.port = int(args.port)
        if args.user:
            self.user = args.user

        if args.file:
            self.config_file = os.path.join(args.file)
        elif len(opts) == 0:
            self.config_file = self.get_config_file()
        else:
            self.config_file = os.path.join(self.config_dir, opts[0])

        try:
            self.read_file(open(self.config_file))
        except DuplicateSectionError as dse:
            print("\nError: Duplicate Section found", file = sys.stderr)
            print("\nException: {0}".format(dse), file = sys.stderr)
            sys.exit(1)
        except FileNotFoundError:
            print("\nError: File '{0}' not found".format(self.config_file), file = sys.stderr)
            sys.exit(1)
        except:
            print("Error: Unexpected error: ", sys.exc_info()[0])
            sys.exit(1)

        self.create_commands()
        self.execute_commands(test_only = args.test)


if __name__ == "__main__":

    cpf = CopyFiles()
    cpf.run_as_tool()
